import facebook from "./facebook.svg";
import instagram from "./instagram.svg";
import linkedin from "./linkedin.svg";
import hero_image1 from "./hero_image1.jpeg";
import hero_image2 from "./hero_image2.jpeg";
import hero_image3 from "./hero_image3.jpeg";
import hero_image4 from "./hero_image4.jpeg";
import hero_image5 from "./hero_image5.jpeg";
import hero_image6 from "./hero_image6.jpeg";
import hero_image7 from "./hero_image7.jpeg";
import hero_image8 from "./hero_image8.jpeg";
import sparklogoblue from './sparklogoblue.svg';
import sparklogowhite from './sparklogowhite.svg';
import menu from './menu.svg';
import close from './close.svg';
import hero from './hero.jpg';
import waves from './waves.svg';
import background_waves from './background_waves.svg';
import uglogo from './ug-logo.svg';
import igem from './igem.png';
import review_wave_1 from './review_wave_1.svg'
import review_wave_2 from './review_wave_2.svg'
import review_wave_3 from './review_wave_3.svg'

export {
  facebook,
  instagram,
  linkedin,
  hero_image1,
  hero_image2,
  hero_image3,
  hero_image4,
  hero_image5,
  hero_image6,
  hero_image7,
  hero_image8,
  sparklogoblue,
  sparklogowhite,
  menu,
  close,
  hero,
  waves,
  background_waves,
  uglogo,
  igem,
  review_wave_1,
  review_wave_2,
  review_wave_3
};
