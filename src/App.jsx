import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { Home, Clients, About, FooterLower, Navbar, Mentorzy, Opinie, Aktualnosci, Kontakt, Calendar } from './components';
import styles from './style';

const App = () => (
    <Router>
      <div className="flex flex-col justify-between">
        <div className={`${styles.paddingX} ${styles.flexCenter} sticky top-0 bg-primary border-b`}>
          <div className={`${styles.boxWidth}`}>
            <Navbar />
          </div>
        </div>
        <div>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/mentorzy" element={<Mentorzy />} />
            <Route path="/czlonkowie" element={<About />} />
            <Route path="/mentees" element={<Clients />} />
            <Route path="/opinie" element={<Opinie />} />
            <Route path="/aktualnosci" element={<Aktualnosci />} />
            <Route path="/kontakt" element={<Kontakt />} />
            <Route path="/kalendarz" element={<Calendar />} />
          </Routes>
        </div>
        <div>
          <FooterLower />
        </div>
      </div>
    </Router>
  );

export default App