import { useState } from 'react';
import { sparklogoblue, close, menu } from '../assets';
import { navLinks } from '../constants';
import { NavLink } from "react-router-dom";

const Navbar = () => {
  const [toggle, setToggle] = useState(false)

  return (
    <nav className="w-full flex py-6 justify-between items-center navbar">
      <NavLink to="/">
        <img src={sparklogoblue} alt="spark logo" className='w-[124px] h-[32px]' />
      </NavLink>

      <ul className='list-none sm:flex hidden justify-end items-center flex-1'>
        {navLinks.map((nav, index) => (
          <li
            key={nav.id}
            className={`font-poppins font-normal text-[16px] ${index === navLinks.length - 1 ? 'mr-0' : 'mr-10'}`}
          >
            <NavLink to={`/${nav.id}`}>
              {nav.title}
            </NavLink>
          </li>
        ))}
      </ul>

      <div className='sm:hidden flex flex-1 justify-end items-center'>
          <img 
            src={toggle ? close : menu} 
            alt="menu" 
            className="w-[28px] h-[28px]
            object-contain"
            onClick={() => setToggle((prev) => !prev)}
          />

          <div
            className={`${toggle ? 'flex' : 'hidden'} p-6 absolute top-20 right-0 mx-4 my-2 min-w-[140px] rounded-xl sidebar bg-primary`}
          >
            <ul className='list-none flex flex-col justify-end items-center flex-1'>
              {navLinks.map((nav, index) => (
                <li
                  key={nav.id}
                  className={`font-poppins font-normal text-[16px] ${index === navLinks.length - 1 ? 'mr-0' : 'mb-4'}`}
                >
                  <NavLink to={`/${nav.id}`} onClick={() => setToggle((prev) => !prev)}>
                    {nav.title}
                  </NavLink>
                </li>
              ))}
            </ul>
          </div>
      </div>
    </nav>
  )
}

export default Navbar
