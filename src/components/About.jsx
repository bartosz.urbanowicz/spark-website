import React from 'react';
import { FooterUpper } from '.';
import { czlonkowie } from '../constants';

const About = () => {
  return (
    <div>
      <div className='grid sm:grid-cols-4 gap-16 sm:m-16 m-8'>
        {czlonkowie.map((czlonek) =>
          <div key={czlonek.id}>
            <img src={czlonek.obraz} alt="person" className='h-[80%] max-w-full rounded-lg object-cover'/>
            <div className='mt-2'>
              <div className='font-bold text-xl'>{czlonek.imie}</div>
              {czlonek.role.map((rola) =>
                <div key={rola}>
                  {rola}
                </div>
              )}
              <div>{czlonek.kierunek}</div>
            </div>
          </div>
        )}
      </div>
      <FooterUpper />
    </div>
  )
}

export default About
