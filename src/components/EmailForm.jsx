import React, { useRef } from 'react';
import emailjs from '@emailjs/browser';

const EmailForm = () => {

    const form = useRef();

    const sendEmail = (e) => {
        e.preventDefault();

        emailjs.sendForm('service_dh9xzfi', 'template_d05i4oj', form.current, 'pUhz2C3nknNPVqGjh')
        .then((result) => {
            // console.log(result.text);
            e.target.reset();
        }, (error) => {
            console.log(error.text);
        });
    };

    return (
        <div className='flex justify-center'>
            <form ref={form} /*onSubmit={sendEmail}*/ className='sm:grid gap-5 w-[50em] flex flex-col items-center'>
                <div className='col-span-2 text-2xl'> Wyślij nam wiadomość! </div>
                <input type="text" placeholder="Twoje imię" spellCheck="false" name="user_name" className='bg-gray-200 p-3 outline-none w-[15rem] sm:w-full'/>
                <input type="email" placeholder="Adres e-mail" spellCheck="false" name="user_email" className='bg-gray-200 p-3 outline-none w-[15rem] sm:w-full' />
                <textarea placeholder="Wiadomość" name="message" className='bg-gray-200 p-3 col-span-2 h-[10em] outline-none w-[15rem] sm:w-full'/>
                <input type="submit" placeholder="Wiadomość" spellCheck="false" value="Wyślij" className='bg-secondary p-3 col-span-2 text-white font-bold cursor-pointer hover:bg-dimSecondary w-[15rem] sm:w-full'/>
            </form>
        </div>
  );
};

export default EmailForm