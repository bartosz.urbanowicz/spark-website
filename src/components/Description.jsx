import { descriptionParagraphs } from "../constants"

const Description = () => {
  return (
    <div>
      {
      descriptionParagraphs.map((paragraph, index) => {
        // switch the icon with text every 2 paragraphs
        if (index % 2 === 0){
          return (
            <div className='my-[5em] flex justify-start items-center' key={index}>
              <div className="bg-primaryBlue min-w-[4rem] h-[4rem] text-white flex justify-center items-center rounded-full">
                <i className={`${paragraph.icon}`}></i>
              </div>
              <div className="ml-[3rem]">
                <h1 className="text-2xl">{paragraph.title}</h1>
                <div>
                  {paragraph.text}
                </div>
              </div>
            </div> 
            )
        }
        else {
          return(
            <div className='my-[5em] flex justify-start items-center' key={index}>
              <div className="mr-[3rem]">
                <h1 className="text-2xl">{paragraph.title}</h1>
                <div>
                  {paragraph.text}
                </div>
              </div>
              <div className="bg-primaryBlue min-w-[4rem] h-[4rem] text-white flex justify-center items-center rounded-full">
                <i className={`${paragraph.icon}`}></i>
              </div>
            </div>
          )
        }
      })
    }
    </div>
    
  )
}

export default Description
