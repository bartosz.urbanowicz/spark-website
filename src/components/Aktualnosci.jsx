import React from 'react'
import { FooterUpper } from '.';
import { InstagramEmbed } from 'react-social-media-embed';
import { socialMediaPosts } from '../constants'

const Aktualnosci = () => {
  return (
    <div>
      {socialMediaPosts.map((post) => 
      <div key={post.id} className='flex justify-center mt-[3em]'>
        <InstagramEmbed url="https://www.instagram.com/p/CvJzdpYINkN/?img_index=1" width={640} />
      </div>
      )}
      <FooterUpper />
    </div>
  )
}

export default Aktualnosci
