import Clients from "./Clients";
import Description from "./Description";
import FooterLower from "./FooterLower";
import FooterUpper from "./FooterUpper";
import Hero from "./Hero";
import Navbar from "./Navbar";
import Mentorzy from "./Mentorzy";
import Home from "./Home";
import About from "./About";
import Opinie from "./Opinie";
import Aktualnosci from "./Aktualnosci";
import Kontakt from "./Kontakt";
import EmailForm from "./EmailForm";
import Calendar from "./Calendar";



export {
    Clients,
    Description,
    FooterLower,
    FooterUpper,
    Hero,
    Navbar,
    Mentorzy,
    Home,
    About,
    Aktualnosci,
    Kontakt,
    Opinie,
    EmailForm,
    Calendar
};