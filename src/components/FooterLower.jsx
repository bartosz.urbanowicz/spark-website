import React from 'react'
import { socialMedia } from '../constants'

const FooterLower = () => {
  return (
    <div>
      <div className='h-[3em] bg-neutral-900 text-gray-500 font-bold flex justify-evenly items-center '>
        <div>
          Spark Gdańsk 2023
        </div>
        <div className='sm:text-3xl text-base'>
          { socialMedia.map((media, index) =>
              <a  key={media.id} href={media.link} className={`${index === 0 || index === socialMedia.length - 1 ? '' : 'm-[1em]'}`}>
                <i className={`${media.icon}`}></i>
              </a>
            ) }
        </div>
      </div>
    </div>
  )
}

export default FooterLower
