import React from 'react'
import { czlonkowie } from '../constants'
import { FooterUpper } from '.';


const Mentorzy = () => {
  return (
    <div>
      <div className='grid sm:grid-cols-4 gap-16 sm:m-16 m-8'>
        {czlonkowie
        .filter(czlonek => czlonek.role.includes("Mentor"))
        .map((czlonek) =>
            <div key={czlonek.id}>
              <img src={czlonek.obraz} alt="person" className='h-[80%] max-w-full rounded-lg object-cover'/>
              <div className='mt-2'>
                <div className='font-bold text-xl'>{czlonek.imie}</div>
                {czlonek.role.map((rola) =>
                  <div key={rola}>
                    {rola}
                  </div>
                )}
                <div>{czlonek.kierunek}</div>
              </div>
            </div>
        )}
      </div>
      <FooterUpper />
    </div>
  )
}

export default Mentorzy
