import React from 'react';
import { opinie } from '../constants';
import { FooterUpper } from '.';

import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';

import { Pagination } from 'swiper/modules';
import { Navigation } from 'swiper/modules';
import { Swiper, SwiperSlide } from 'swiper/react';

import { review_wave_1, review_wave_2, review_wave_3 } from '../assets'

const Opinie = () => {
  return (
    <div>
      <Swiper
        spaceBetween={50}
        slidesPerView={3}
        pagination={{
          dynamicBullets: true,
        }}
        navigation={true}
        modules={[Pagination, Navigation]}
        // onSlideChange={() => console.log('slide change')}
        onSwiper={(swiper) => console.log(swiper)}
        className='w-[71%]'
      >
        <div>
          {opinie.map((opinia) =>
            <SwiperSlide key={opinia.id} className='h-[30em] border py-[1em] px-[4em] mt-[1em] select-none'>
              <div>
                <div className='flex'>
                  <img src={opinia.icon} alt="person" className='h-[50px] w-[50px] m-2 rounded-full object-cover'/>
                  <div className='flex-col justify-center items-center'>
                    <div className='mt-1 font-bold'>{opinia.name}</div>
                    <div>{opinia.title}</div>
                  </div>
                </div>
                {opinia.content}
              </div>
              <img src={review_wave_1} alt="" className='w-[100%]'/>
            </SwiperSlide>
          )}
        </div>
      </Swiper>
      <FooterUpper />
    </div>
  )
}

export default Opinie
