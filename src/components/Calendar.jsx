import React from 'react';
import FooterUpper from './FooterUpper';
import { events } from '../constants';
import Collapsible from 'react-collapsible';

const Calendar = () => {
  return (
    <div>
      <div className='flex justify-center'>
        <div className='w-[50em]'>
          <h1 className='font-bold text-xl my-[1em]'>Nadchodzące eventy</h1>
            <div>
              <hr></hr>
              {events.map((event) => 
                <Collapsible key={event.key} 
                trigger=
                  {<div>
                    <div className='flex py-4'>
                      <div className='border-l-4 border-blue-500 px-4 w-[7em]'>
                        <div className='font-bold text-xl text-center'>{event.date.day}</div>
                        <div className='text-center'>{event.date.month}</div>
                        <div className='text-center text-sm text-gray-500 hidden'>{event.date.year}</div> 
                      </div>
                      <div>
                        <div className='text-xl'>{event.title}</div>
                        <div className='text-gray-500'>{event.place}</div>                  
                      </div>
                    </div>
                    <hr></hr>
                  </div>}
                >
                  <div>
                    {event.description}
                  </div>
                </Collapsible>
                
                )}
            </div>
        </div>
      </div>
      <FooterUpper />
    </div>
    
  )
}

export default Calendar
