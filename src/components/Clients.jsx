import React from 'react'
import { mentees } from '../constants';

const Clients = () => {
  return (
    <div className='bg-bg-waves bg-cover sm:p-16 p-8 flex justify-center'>
      <div className='grid sm:grid-cols-3 gap-16 mb-[6em]'>
        {mentees.map((mentee) =>
            <div key={mentee.id}>
              <a href={mentee.link}>
                <img src={mentee.img} alt="logo" className='h-[20em] max-[20em] rounded-lg object-cover'/>
              </a>
              
            </div>
          )}
      </div>
    </div>
  )
}

export default Clients
