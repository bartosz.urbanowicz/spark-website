import styles from '../style';
import { Description, Hero, FooterUpper } from '.';

const Home = () => (
  <div className='bg-primary w-full overflow-hidden'>
    <div className={`bg-hero1 bg-cover ${styles.flexStart} h-[73vh]`}>
      <div className={`${styles.boxWidth}`}>
        <Hero />
      </div>
    </div>

    <div className={`bg-primary ${styles.paddingX} ${styles.flexStart}`}>
      <div className={`${styles.boxWidth}`}>
        <Description />
      </div>
    </div>
    <FooterUpper />
  </div>
)

export default Home
