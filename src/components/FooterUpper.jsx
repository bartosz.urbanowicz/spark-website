import React from 'react'
import { footerLogos } from '../constants'

const FooterUpper = () => {
  return (
    <div>
      <div className="bg-waves2 bg-cover h-[60vh] flex justify-evenly items-center text-white">
        {footerLogos.map(logo => (
          <div key={logo.id} className='w-[8em] h-[8em] sm:w-[16em] sm:h-[16em] flex justify-center'>
            <img src={logo.img} alt={logo.alt} />
          </div>
        ))}
      </div>
    </div>
  ) 
}

export default FooterUpper
