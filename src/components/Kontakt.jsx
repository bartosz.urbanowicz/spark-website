import EmailForm from './EmailForm'
import { daneKontaktowe } from '../constants';
import { socialMedia } from '../constants';
import { FooterUpper } from '.';

const Kontakt = () => {
  return (
    <div>
      <div className='h-[10em] flex justify-center'> 
        <div className='sm:w-[50em]'>
          <div className='text-2xl my-2'>Kontakt</div>
          { daneKontaktowe.map((kontakt) =>
            <div key={kontakt.id}>
              <i className={`${kontakt.icon} mx-1 my-2`}></i>
              { kontakt.content }
            </div>
          ) }
          <div className='mx-1 text-xl my-2'>
            { socialMedia.map((media, index) =>
              <a  key={media.id} href={media.link} className={`${index === 0 || index === socialMedia.length - 1 ? '' : 'm-[1em]'}`}>
                <i className={`${media.icon}`}></i>
              </a>
            ) }
          </div>
        </div>
      </div>
      <EmailForm/>
      <FooterUpper />
    </div>
  )
}

export default Kontakt
