import { uglogo, sparklogowhite, igem } from '../assets';

export const navLinks = [
  {
    id: "mentorzy",
    title: "Mentorzy",
  },
  {
    id: "czlonkowie",
    title: "Członkowie",
  },
  {
    id: "mentees",
    title: "Mentees"
  },
  {
    id: "opinie",
    title: "Opinie"
  },
  {
    id: "aktualnosci",
    title: "Aktualności",
  },
  {
    id: "kalendarz",
    title: "Kalendarz",
  },
  {
    id: "kontakt",
    title: "Kontakt",
  }
];

export const daneKontaktowe = [
  // {
  //   id: "kontakt-1",
  //   icon: "fa-solid fa-phone",
  //   content: "123456789",
  // },
  {
    id: "kontakt-2",
    icon: "fa-solid fa-envelope",
    content: "email@email.com",
  },
];

export const socialMedia = [
  {
    id: "social-media-1",
    icon: "fa-brands fa-instagram",
    link: "https://www.instagram.com/spark_gdansk/",
  },
  {
    id: "social-media-2",
    icon: "fa-brands fa-facebook",
    link: "https://www.facebook.com/profile.php?id=100088414040168",
  },
  {
    id: "social-media-3",
    icon: "fa-brands fa-linkedin",
    link: "https://www.linkedin.com/company/spark-gda%C5%84sk/",
  },
];

export const czlonkowie = [
  {
    id: "czlonek-1",
    obraz: "/src/assets/image7.jpeg",
    imie: "Marianna Pilecka",
    role: ["Lider", "Mentor"],
    kierunek: "International Buisness (2)"
  },
  {
    id: "czlonek-2",
    obraz: "/src/assets/image1.jpeg",
    imie: "Paweł Ramotowski",
    role: ["Mentor", "Spark*Law"],
    kierunek: "Prawo (2)"
  },
  {
    id: "czlonek-3",
    obraz: "/src/assets/image11.jpeg",
    imie: "Adam Sorrell",
    role: ["Mentor"],
    kierunek: "International Buisness (2)"
  },
  {
    id: "czlonek-4",
    obraz: "/src/assets/image0.jpeg",
    imie: "Klaudia Kreft",
    role: ["Social media", "Rekrutacja"],
    kierunek: "Zarządzanie (1)"
  },
  {
    id: "czlonek-5",
    obraz: "/src/assets/image9.jpeg",
    imie: "Milena Kot",
    role: ["Mentor"],
    kierunek: "Chemia (3)"
  },
  {
    id: "czlonek-5",
    obraz: "/src/assets/image10.jpeg",
    imie: "Aleksandra Południewska",
    role: ["Mentor"],
    kierunek: "Brand and communications management (5)"
  },
  {
    id: "czlonek-6",
    obraz: "/src/assets/image8.jpeg",
    imie: "Wiktoria Klinkosz",
    role: ["Mentor"],
    kierunek: "Ekonomia (1)"
  },
  {
    id: "czlonek-7",
    obraz: "/src/assets/image2.jpeg",
    imie: "Kordian Dziwisz",
    role: ["Mentor"],
    kierunek: "Informatyka Praktyczna (2)"
  },
  {
    id: "czlonek-8",
    obraz: "/src/assets/image4.jpeg",
    imie: "Adrianna Franczak",
    role: ["Dawniej: mentor"],
    kierunek: "Ekonomia (ukończone)"
  },
  {
    id: "czlonek-9",
    obraz: "/src/assets/image5.jpeg",
    imie: "Adam Jurkiewicz",
    role: ["Dawniej: mentor"],
    kierunek: "Biologia morska i oceanografia (ukończone)"
  },
  {
    id: "czlonek-10",
    obraz: "/src/assets/image3.jpeg",
    imie: "Wiktoria Chudzik",
    role: ["Dawniej: mentor"],
    kierunek: "Oceanografia/biologia (ukończone)"
  }

]

export const opinie = [
  {
    id: "opinia-1",
    content:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, minima quis! Ipsam soluta doloremque ducimus, veritatis eum nihil blanditiis voluptate.",
    name: "Herman Jensen",
    title: "Founder & Leader",
    icon: "/src/assets/default_person.png"
  },
  {
    id: "opinia-2",
    content:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis, nobis.",
    name: "Herman Jensen",
    title: "Founder & Leader",
    icon: "/src/assets/default_person.png"
  },
  {
    id: "opinia-3",
    content:
      "Lorem ipsum dolor sit amet.",
    name: "Herman Jensen",
    title: "Founder & Leader",
    icon: "/src/assets/default_person.png"
  },
  {
    id: "opinia-4",
    content:
      "Lorem ipsum dolor sit amet.",
    name: "Herman Jensen",
    title: "Founder & Leader",
    icon: "/src/assets/default_person.png"
  },
  {
    id: "opinia-5",
    content:
      "Lorem ipsum dolor sit amet. sfdsfdsfsdfsds",
    name: "Herman Jensen",
    title: "Founder & Leader",
    icon: "/src/assets/default_person.png"
  },
  {
    id: "opinia-6",
    content:
      "Lorem ipsum dolor sit amet. basdkksfsdvvsd",
    name: "Herman Jensen",
    title: "Founder & Leader",
    icon: "/src/assets/default_person.png"
  }
];

export const mentees = [
  {
    id: "mentee-1",
    title: "Igem",
    link: "https://www.youtube.com/watch?v=9Wm0kyjtmaE",
    img: igem
  },
  {
    id: "mentee-2",
    title: "Swim",
    link: "https://www.youtube.com/watch?v=9Wm0kyjtmaE",
    img: "src/assets/default_mentee.png"
  },
  {
    id: "mentee-3",
    title: "Swim",
    link: "https://www.youtube.com/watch?v=9Wm0kyjtmaE",
    img: "src/assets/default_mentee.png"
  },
  {
    id: "mentee-4",
    title: "Swim",
    link: "https://www.youtube.com/watch?v=9Wm0kyjtmaE",
    img: "src/assets/default_mentee.png"
  },
  {
    id: "mentee-5",
    title: "Swim",
    link: "https://www.youtube.com/watch?v=9Wm0kyjtmaE",
    img: "src/assets/default_mentee.png"
  },
  {
    id: "mentee-6",
    title: "Swim",
    link: "https://www.youtube.com/watch?v=9Wm0kyjtmaE",
    img: "src/assets/default_mentee.png"
  },
];

export const footerLogos = [
  {
    id: "logo-1",
    title: "Uniwersytet Gdański",
    alt: "logo Uniwersytetu Gdańskiego",
    img: uglogo,
  },
  {
    id: "logo-2",
    title: "Spark",
    alt: "logo Sparka",
    img: sparklogowhite,
  }
];

export const events = [
  {
    id: "event-1",
    title: "Event",
    date: {
      day: 1,
      month: "Styczeń",
      year: 2023
    },
    place: "Radom",
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Qui numquam eos dignissimos corporis odio, inventore facilis voluptatem non sunt nesciunt."
  },
  {
    id: "event-2",
    title: "Event 2",
    date: {
      day: 23,
      month: "Lipiec",
      year: 1984
    },
    place: "Chrobrego 82D 1 Gdańsk",
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Qui numquam eos dignissimos corporis odio, inventore facilis voluptatem non sunt nesciunt."
  },
  {
    id: "event-3",
    title: "Event 3",
    date: {
      day: 1,
      month: "Październik",
      year: 2003
    },
    place: "Szkoła Podstawowa nr48 Gdańsk",
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Qui numquam eos dignissimos corporis odio, inventore facilis voluptatem non sunt nesciunt."
  }
]

export const socialMediaPosts = [
  {
    id: "post-1",
    url: "https://www.instagram.com/p/CvJzdpYINkN/?img_index=1"
  },
  {
    id: "post-2",
    url: "https://www.instagram.com/p/CvJzdpYINkN/?img_index=1"
  },
  {
    id: "post-3",
    url: "https://www.instagram.com/p/CvJzdpYINkN/?img_index=1"
  },
  {
    id: "post-4",
    url: "https://www.instagram.com/p/CvJzdpYINkN/?img_index=1"
  }
]

export const descriptionParagraphs = [
  {
    icon: "fa-solid fa-lightbulb",
    title: "Title",
    text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem incidunt enim velit distinctio officiis ut neque tempora ratione veniam! Neque reprehenderit fugit laborum doloribus atque totam laboriosam ab saepe repudiandae. Modi, ex velit numquam facilis blanditiis ipsa quidem incidunt quaerat qui eos ut error tempora magnam iusto, dolorem quis! Harum vitae voluptates nulla necessitatibus sed similique iste quasi soluta dolores? Dolore, esse voluptatum! Corrupti dolores exercitationem at consequuntur quis, molestiae nisi porro atque explicabo eum error recusandae nulla ea. Dignissimos dolor eius delectus enim eos, vero ad exercitationem odio rerum!"
  },
  {
    icon: "fa-solid fa-lightbulb",
    title: "Title2",
    text: "Lorem ipsum dolor sit amsdgdgfdhfd consectetur adipisicing elit. Autem incidunt enim vesdgfdfhsasffit distinctio officiis ut neque tempora ratione veniam! Neque reprehenderit fugit laborum doloribus atque totam laboriosam ab saepe repudiandae. Modi, ex velit numquam facilis blanditiis ipsa quidem incidunt quaerat qui eos ut error tempora magnam iusto, dolorem quis! Harum vitae voluptates nulla necessitatibus sed similique iste quasi soluta dolores? Dolore, esse voluptatum! Corrupti dolores exercitationem at consequuntur quis, molestiae nisi porro atque explicabo eum error recusandae nulla ea. Dignissimos dolor eius delectus enim eos, vero ad exercitationem odio rerum!"
  },
  {
    icon: "fa-solid fa-lightbulb",
    title: "Title3",
    text: "Lorem ipsum dolor sit amet, consectetur adipisicing gdfdhgfjjjjjjjf Autem incidunt enim velit distinctio officiis ut neque tempora ratione veniam! Neque reprehenderit fugit laborum doloribus atque totam laboriosam ab saepe repudiandae. Modi, ex velit numquam facilis blanditiis ipsa quidem incidunt quaerat qui eos ut error tempora magnam iusto, dolorem quis! Harum vitae voluptates nulla necessitatibus sed similique iste quasi soluta dolores? Dolore, esse voluptatum! Corrupti dolores exercitationem at consequuntur quis, molestiae nisi porro atque explicabo eum error recusandae nulla ea. Dignissimos dolor eius delectus enim eos, vero ad exercitationem odio rerum!"
  },
]