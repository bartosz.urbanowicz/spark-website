/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{js,jsx}"],
  mode: "jit",
  theme: {
    extend: {
      colors: {
        primary: "white",
        secondary: "#0041d2",
        dimSecondary: "#03329c",
        dimWhite: "rgba(255, 255, 255, 0.7)",
        dimBlue: "rgba(9, 151, 124, 0.1)",
        primaryBlue: "rgb(0 65 210)"
      },
      fontFamily: {
        poppins: ["Poppins", "sans-serif"],
      },
      backgroundImage: {
        'hero1': "url('/src/assets/hero_image1.jpeg')",
        'hero2': "url('/src/assets/hero_image2.jpeg')",
        'waves': "url(/src/assets/waves.svg)",
        'waves2': "url(/src/assets/waves2.svg)",
        'bg-waves': "url(/src/assets/background_waves.svg)",
        'review_wave_1' : "url(/src/assets/review_wave_1.svg)",
        'review_wave_2' : "url(/src/assets/review_wave_2.svg)",
        'review_wave_3' : "url(/src/assets/review_wave_3.svg)",
      }
    },
    screens: {
      xs: "480px",
      ss: "620px",
      sm: "768px",
      md: "1060px",
      lg: "1200px",
      xl: "1700px",
    },
  },
  plugins: [],
};